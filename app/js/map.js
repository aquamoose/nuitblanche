function initialize() {
    var center = new google.maps.LatLng(25.072847, 121.524841);
    var mapOptions = {
        zoom: 15,
        center: center,
    };
    var infowindow = new google.maps.InfoWindow();
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    google.maps.event.addListener(map, "click", function(event) {
      infowindow.close();
    });
    var thisURL = "../map/north_all_type-all_page1.json";
    function defaultSetting() {
      $(".pagination span").removeClass('page-active');
      $(".pagination span:first-child").addClass('page-active');
      $(".infoPanel-box.row").html("");
    }
    $(".nav-item").each(function() {
      $(this).click(function() {
        var locationTag = $(this).attr("id");
        if(locationTag == "nav-north-tab") {
          var thisURL = "../map/north_all_type-all_page1.json";
          defaultSetting();
          dataPush(thisURL);
        } else {
          var thisURL = "../map/south_all_type-all_page1.json";
          defaultSetting();
          dataPush(thisURL);
        }
      });
    });
    $(".north-location, .north-type-filter").each(function() {
      $(this).click(function() {
        var areaTag = $(".north-location.location-active").attr("tag");
        var typeTag = $(".north-type-filter.type-active").attr("tag");
        var thisURL = "../map/north_" + areaTag + "_" + typeTag + "_page1.json";
        defaultSetting();
        dataPush(thisURL);
      });
    });
    $(".south-location, .south-type-filter").each(function() {
      $(this).click(function() {
        var areaTag = $(".south-location.location-active").attr("tag");
        var typeTag = $(".south-type-filter.type-active").attr("tag");
        var thisURL = "../map/south_" + areaTag + "_" + typeTag + "_page1.json";
        defaultSetting();
        dataPush(thisURL);
      });
    });
    $(".pagination span").each(function() {
      $(this).click(function() {
        $(".pagination span").removeClass('page-active');
        $(this).addClass('page-active');
        var areaTag = $(".location-active").attr("tag");
        var typeTag = $(".type-active").attr("tag");
        var pageTag = $(this).attr("pageno");
        var locationTag = $(this).parent().parent().parent().find('.nav-item.active').attr("id");
        if(locationTag == "nav-north-tab") {
          var thisURL = "../map/north_" + areaTag + "_" + typeTag + "_page" + pageTag + ".json";
          $(".infoPanel-box.row").html("");
          dataPush(thisURL);
        } else if(locationTag == "nav-south-tab") {
          var thisURL = "../map/south_" + areaTag + "_" + typeTag + "_page" + pageTag + ".json";
          $(".infoPanel-box.row").html("");
          dataPush(thisURL);
        }
      });
    });
    dataPush(thisURL);
    function dataPush(_url) {
      $.ajax({
        url: _url,
        cache: false,
        dataType: "json",
        success: function(data) {
          $.each(data, function(key, item) {
              var latLng = new google.maps.LatLng(item.latitude, item.longitude);
              var icon = {
                url: item.marker,
                scaledSize: new google.maps.Size(30,30)
              }
              var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                title: item.title,
                icon: icon
              });
              var infoPanelContent = '<div class="infoPanel-item-wrap col-lg-6 col-sm-12"><div class="infoPanel-item infoPanel-item-' + item.colorTag + '"><div class="infoPanel-indicator"><img src="' + item.marker + '"><p>' + item.typeContent + '</p></div><div class="infoPanel-description"><h5>' + item.title + '</h5><p class="infoPanel-artist">' + item.artist + '</p><p class="infoPanel-location">【地點】' + item.locationContent + '</p><p class="infoPanel-time">【時間】' + item.timeContent + '</p></div><div class="infoPanel-link"><div><a href="' + item.gMap + '">Google Map</a><a href="' + item.detail + '">查看介紹</a></div></div></div></div>';
              (function(marker, data) {
              google.maps.event.addListener(marker, "click", function(e) {
                infowindow.setContent('<div class="gm-info-window"><img src="' + item.infoWindowImage + '"><p class="gm-info-window-header">'+item.title+'</p><p>藝術家: ' + item.artist + '</p><p>'+item.description+'</p><div class="gm-info-window-link"><a href="' + item.gMap + '">Google Map</a><a href="' + item.detail + '">作品介紹</a></div></div>');
                infowindow.open(map, marker);
                map.panTo(this.getPosition());
                map.panBy(0,-200);
              });
              $(".infoPanel-box.row").append(infoPanelContent);
            })(marker, item);
          });
        }
    });
  }
}
function initialize2() {
  var center2 = new google.maps.LatLng(25.072847, 121.524841);
    var mapOptions = {
        zoom: 16,
        center: center2,
    };
    var infowindow2 = new google.maps.InfoWindow();
    var map2 = new google.maps.Map(document.getElementById('access-map'), mapOptions);
    google.maps.event.addListener(map2, "click", function(event) {
      infowindow.close();
    });
    var thisURL2 = "../map/north_all_type-all_page2.json";
    dataPush2(thisURL2);
    function dataPush2(_url) {
      $.ajax({
        url: _url,
        cache: false,
        dataType: "json",
        success: function(data) {
          $.each(data, function(key, item) {
              var latLng = new google.maps.LatLng(item.latitude, item.longitude);
              var icon = {
                url: item.marker,
                scaledSize: new google.maps.Size(30,30)
              }
              var marker = new google.maps.Marker({
                position: latLng,
                map: map2,
                title: item.title,
                icon: icon
              });
              (function(marker, data) {
              google.maps.event.addListener(marker, "click", function(e) {
                infowindow.setContent('<div class="gm-info-window"><p class="gm-info-window-header">'+item.title+'</p></div>');
                infowindow.open(map, marker);
                map.panTo(this.getPosition());
                map.panBy(0,-200);
              });
            })(marker, item);
          });
        }
    });
  }
}