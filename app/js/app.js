var Home = Barba.BaseView.extend({
    namespace: "home",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initHome();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var BlobMenu = Barba.BaseView.extend({
    namespace: "blob-menu",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initBlobMenu();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var Timeline = Barba.BaseView.extend({
    namespace: "timeline",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initTimeline();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var Route = Barba.BaseView.extend({
    namespace: "route",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initRoute();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var Program = Barba.BaseView.extend({
    namespace: "program",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initProgram();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var Category = Barba.BaseView.extend({
    namespace: "category",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initCategory();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var Work = Barba.BaseView.extend({
    namespace: "work",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initWork();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var About = Barba.BaseView.extend({
    namespace: "about",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initAbout();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
var Access = Barba.BaseView.extend({
    namespace: "access",
    onEnter: function () {},
    onEnterCompleted: function () {
    	preloaderTimeline();
        initAccess();
    },
    onLeave: function () {},
    onLeaveCompleted: function () {}
});
function queue(start) {
	var rest = [].splice.call(arguments, 1),
	promise = $.Deferred();

	if (start) {
		$.when(start()).then(function () {
		    queue.apply(window, rest);
	});
} else {
	promise.resolve();
}
	return promise;
}
function preloaderTimeline() {
	$(window).load(function(){
		var dUp = document.querySelector('.loader-bottom');
	    var dDown = document.querySelector('.loader-top');
	    var loadUp = anime({
		  targets: dUp,
		  height: '0%',
		  easing: 'easeInSine',
		  duration: 1500,
		});
		var loadDown = anime({
		  targets: dDown,
		  height: '0%',
		  easing: 'easeInSine',
		  duration: 1500,  
		});
	});
}
$(function () {
	initGlobal();
	Home.init();
	BlobMenu.init();
	Timeline.init();
	Route.init();
	Program.init();
	Category.init();
	Work.init();
	About.init();
	Access.init();
	Barba.Pjax.init();
    Barba.Prefetch.init();
    var FadeTransition = Barba.BaseTransition.extend({
	  start: function() {
	    Promise
	      .all([this.fadeOut(), this.newContainerLoading])
	      .then(this.fadeIn.bind(this));
	  },
	 
	  fadeOut: function() {
	    return new Promise(function(resolve){
		    var dDown = document.querySelector('.loader-top');
		    anime({
			  targets: dDown,
			  height: '50%',
			  easing: 'easeInSine',
			  duration: 1000
			});
			var dUp = document.querySelector('.loader-bottom');
		    anime({
			  targets: dUp,
			  height: '50%',
			  easing: 'easeInSine',
			  duration: 1000,
			  complete: function(){
			  	resolve()
			  }
			});
	    })
	  },

	  fadeIn: function() {
		var _this = this;
		var dUp = document.querySelector('.loader-bottom');
	    var dDown = document.querySelector('.loader-top');
	    $(this.oldContainer).hide();
	    var loadUp = anime({
		  targets: dUp,
		  height: '0%',
		  easing: 'easeInSine',
		  duration: 1500,
		});
		var loadDown = anime({
		  targets: dDown,
		  height: '0%',
		  easing: 'easeInSine',
		  duration: 1500,
		});
		_this.done();
	  }
	});
    Barba.Pjax.getTransition = function() {
 		return FadeTransition;
	};
	
	Barba.Pjax.cacheEnabled = false;

});

function initGlobal() {
	$(window).load(function(){
		$('.content').removeClass('content-loading');
	});
	$(".mobile-menu-button").click(function(){
		$(this).hide();
		$(".mobile-header").fadeIn('300');
		$('body').addClass('no-scroll');
	});
	$(".mobile-menu-button-close").click(function() {
		$(this).parent().fadeOut(300);
		$(".mobile-menu-button").show();
		$('body').removeClass('no-scroll');
	});
	function headerShrink() {
		var scroll = $(window).scrollTop();
		if (scroll > 50 && $(".barba-container").attr("data-namespace") == "home") {
			$("header").removeClass('header-bg-hack').addClass('shrink');
			$(".header-time").removeClass('header-time-alt');
		} else if($(".barba-container").attr("data-namespace") != "home") {
			$("header").removeClass('header-bg-hack').addClass('shrink');
			$(".header-time").removeClass('header-time-alt');
		} else {
			$("header").removeClass('shrink');
		}
	}
	$(document).ready(function(){
		headerShrink();
		$(window).scroll(function(){
			headerShrink();
		});
		var url = window.location.hash;
		if(location.href.indexOf("#") != -1) {
		  $(window).animate({
		  	scrollTop: $(url).offset().top - 150
		  });
		} else {
			$(window).scrollTop(0);
		}
		/////// footer blob
		Math.TWO_PI = Math.PI * 2;

		const canvas12 = document.createElement('canvas');
		const context12 = canvas12.getContext('2d');

		function loop12(time) {
		    
		    loop12.handle = requestAnimationFrame(loop12);

		    context12.clearRect(0, 0, canvas12.width, canvas12.height);
		    
		    let angle12 = 0;
		    let count12 = 100;
		    let slice12 = Math.TWO_PI / count12;
			let radius12 = Math.min(window.innerWidth * 0.3, $('.footer-circle').width() / 2);
		    let points12 = [];
		    
		    for(let i = 0; i < count12; i++) {

		        let x = Math.cos(angle12);
		        let y = Math.sin(angle12);
		        let displacement = radius12 + (noise.simplex3(x + 2.5, y + 3.5, time * 0.001) * 2);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points12.push({ x, y });
		        angle12 += slice12;
		    }

		    context12.save();
		    context12.translate(canvas12.center.x, canvas12.center.y);
		    context12.beginPath();
		    context12.moveTo(points12[0].x, points12[0].y);

		    curveThrough12(points12, context12);
		    var my_gradient12=context12.createLinearGradient(-80,-80,100,0);
			my_gradient12.addColorStop(0,"#3f2c75");
			my_gradient12.addColorStop(0.3,"#a20d82");
			my_gradient12.addColorStop(0.7,"#e53829");
			my_gradient12.addColorStop(1,"#c7973f");
		    context12.fillStyle = my_gradient12;
		    context12.fill();
		    context12.restore();
		}

		function resize12() {
		    canvas12.width = $(".footer-circle").width() + 20;
		    canvas12.height = $(".footer-circle").height() + 20;
		    canvas12.center.x = canvas12.width * 0.5;
		    canvas12.center.y = canvas12.height * 0.5;
		}

		function curveThrough12(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init12() {

		    canvas12.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize12();
			  });
		    resize12();
		    // color = randomColor();
			$(".footer-time").prepend(canvas12);
		    requestAnimationFrame(loop12);
		}

		init12();
	});
}

function initHome() {
	initGlobal();
	initialize();
	function headerShrink() {
		var scroll = $(window).scrollTop();
		if (scroll > 50) {
			$("header").removeClass('header-bg-hack').addClass('shrink');
			$(".header-time").removeClass('header-time-alt');
		} else if(scroll < 50 && $(".barba-container").attr("data-namespace") != "home") {
			$("header").removeClass('shrink').addClass('header-bg-hack');
			$(".header-time").addClass('header-time-alt');
		} else {
			$("header").removeClass('shrink');
		}
	}
	$(document).ready(function(){
		setTimeout(function(){
			$svg1 = $('#index-logo').drawsvg();
			$svg1.drawsvg('animate');
		}, 2000);
		setTimeout(function(){
			$('.nuit-blanche-logo').fadeIn(1000);
		}, 8000);
		$('.north-location').click(function() {
			$('.north-location.location-active').removeClass('location-active');
			$(this).addClass('location-active');
		});
		$('.north-type-filter').click(function() {
			$('.north-type-filter.type-active').removeClass('type-active');
			$(this).addClass('type-active');
		});
		$(".header-logo-alt").hide();
		$(".header-time").removeClass('header-time-alt');
		$("header").removeClass('header-bg-hack');
		// var countDownDate = new Date("Oct 6, 2018 18:00:00").getTime();

		// // Update the count down every 1 second
		// var x = setInterval(function() {

		//   // Get todays date and time
		//   var now = new Date().getTime();

		//   // Find the distance between now an the count down date
		//   var distance = countDownDate - now;

		//   // Time calculations for days, hours, minutes and seconds
		//   var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		//   var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		//   var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		//   var seconds = Math.floor((distance % (1000 * 60)) / 1000);

		//   // Display the result in the element with id="demo"
		//   document.getElementById("countdown").innerHTML = days + "d " + hours + "h "
		//   + minutes + "m " + seconds + "s ";

		//   // If the count down is finished, write some text 
		//   if (distance < 0) {
		//     clearInterval(x);
		//     document.getElementById("countdown").innerHTML = "";
		//     document.getElementById("countdown").style.display = 'none';
		//   }
		// }, 1000);


		////// Index header blob
		let canvas, ctx;
		let render, init;
		let blob;

		class Blob {
		  constructor() {
		    this.points = [];
		  }
		  
		  init() {
		    for(let i = 0; i < this.numPoints; i++) {
		      let point = new Point(this.divisional * ( i + 1 ), this);
		      // point.acceleration = -1 + Math.random() * 2;
		      this.push(point);
		    }
		  }
		  
		  render() {
		    let canvas = this.canvas;
		    let ctx = this.ctx;
		    let position = this.position;
		    let pointsArray = this.points;
		    let radius = this.radius;
		    let points = this.numPoints;
		    let divisional = this.divisional;
		    let center = this.center;
		    
		    ctx.clearRect(0,0,canvas.width,canvas.height);
		    
		    pointsArray[0].solveWith(pointsArray[points-1], pointsArray[1]);

		    let p0 = pointsArray[points-1].position;
		    let p1 = pointsArray[0].position;
		    let _p2 = p1;

		    ctx.beginPath();
		    ctx.moveTo(center.x, center.y);
		    ctx.moveTo( (p0.x + p1.x) / 2, (p0.y + p1.y) / 2 );

		    for(let i = 1; i < points; i++) {
		      
		      pointsArray[i].solveWith(pointsArray[i-1], pointsArray[i+1] || pointsArray[0]);

		      let p2 = pointsArray[i].position;
		      var xc = (p1.x + p2.x) / 2;
		      var yc = (p1.y + p2.y) / 2;
		      ctx.quadraticCurveTo(p1.x, p1.y, xc, yc);
		      // ctx.lineTo(p2.x, p2.y);
		      var my_gradient=ctx.createLinearGradient(1000,0,0,1000);
				my_gradient.addColorStop(0,"#3f2c75");
				my_gradient.addColorStop(0.3,"#a20d82");
				my_gradient.addColorStop(0.7,"#e53829");
				my_gradient.addColorStop(1,"#c7973f");
		      ctx.fillStyle = my_gradient;
		      // ctx.fillRect(p1.x-2.5, p1.y-2.5, 5, 5);

		      p1 = p2;
		    }

		    var xc = (p1.x + _p2.x) / 2;
		    var yc = (p1.y + _p2.y) / 2;
		    ctx.quadraticCurveTo(p1.x, p1.y, xc, yc);
		    // ctx.lineTo(_p2.x, _p2.y);

		    // ctx.closePath();
		    ctx.fillStyle = this.color;
		    ctx.fill();
		    ctx.strokeStyle = my_gradient;
		    // ctx.stroke();
		    
		/*
		    ctx.fillStyle = '#000000';
		    if(this.mousePos) {
		      let angle = Math.atan2(this.mousePos.y, this.mousePos.x) + Math.PI;
		      ctx.fillRect(center.x + Math.cos(angle) * this.radius, center.y + Math.sin(angle) * this.radius, 5, 5);
		    }
		*/
		    requestAnimationFrame(this.render.bind(this));
		  }
		  
		  push(item) {
		    if(item instanceof Point) {
		      this.points.push(item);
		    }
		  }
		  
		  set color(value) {
		    this._color = value;
		  }
		  // get color() {
		  //   return this._color || my_gradient;
		  // }
		  
		  set canvas(value) {
		    if(value instanceof HTMLElement && value.tagName.toLowerCase() === 'canvas') {
		      this._canvas = canvas;
		      this.ctx = this._canvas.getContext('2d');
		    }
		  }
		  get canvas() {
		    return this._canvas;
		  }
		  
		  set numPoints(value) {
		    if(value > 2) {
		      this._points = value;
		    }
		  }
		  get numPoints() {
		    return this._points || 32;
		  }
		  
		  set radius(value) {
		    if(value > 0) {
		      this._radius = value;
		    }
		  }
		  get radius() {
		    // return this._radius || 230;
		    if($(window).width() < 900 && $(window).width() > 481) {
		    	return this._radius || 153;
		    } else if($(window).width() < 480){
		    	return this._radius || 100;
		    } else {
				return this._radius || 230;
		    }
		  }
		  
		  set position(value) {
		    if(typeof value == 'object' && value.x && value.y) {
		      this._position = value;
		    }
		  }
		  get position() {
		    if($(window).width() > 900 ) {
		    	return this._position || { x: 0.5, y: 0.55 };
		    } else if($(window).width() < 900 && $(window).width() > 480) {
		    	return this._position || { x: 0.5, y: 0.48 };
		    } else {
		    	return this._position || { x: 0.5, y: 0.56 };
		    }
		  }
		  
		  get divisional() {
		    return Math.PI * 2 / this.numPoints;
		  }
		  
		  get center() {
		    return { x: this.canvas.width * this.position.x, y: this.canvas.height * this.position.y };
		  }
		  
		  set running(value) {
		    this._running = value === true;
		  }
		  get running() {
		    return this.running !== false;
		  }
		}

		class Point {
		  constructor(azimuth, parent) {
		    this.parent = parent;
		    this.azimuth = Math.PI - azimuth;
		    this._components = { 
		      x: Math.cos(this.azimuth),
		      y: Math.sin(this.azimuth)
		    };
		    
		    this.acceleration = -0.3 + Math.random() * .5;
		  }
		  
		  solveWith(leftPoint, rightPoint) {
		    this.acceleration = (-0.3 * this.radialEffect + ( leftPoint.radialEffect - this.radialEffect ) + ( rightPoint.radialEffect - this.radialEffect )) * this.elasticity - this.speed * this.friction;
		  }
		  
		  set acceleration(value) {
		    if(typeof value == 'number') {
		      this._acceleration = value;
		      this.speed += this._acceleration * 1;
		    }
		  }
		  get acceleration() {
		    return this._acceleration || 0;
		  }
		  
		  set speed(value) {
		    if(typeof value == 'number') {
		      this._speed = value;
		      this.radialEffect += this._speed * 5;
		    }
		  }
		  get speed() {
		    return this._speed || 0;
		  }
		  
		  set radialEffect(value) {
		    if(typeof value == 'number') {
		      this._radialEffect = value;
		    }
		  }
		  get radialEffect() {
		    return this._radialEffect || 0;
		  }
		  
		  get position() {
		    return { 
		      x: this.parent.center.x + this.components.x * (this.parent.radius + this.radialEffect), 
		      y: this.parent.center.y + this.components.y * (this.parent.radius + this.radialEffect) 
		    }
		  }
		  
		  get components() {
		    return this._components;
		  }

		  set elasticity(value) {
		    if(typeof value === 'number') {
		      this._elasticity = value;
		    }
		  }
		  get elasticity() {
		    return this._elasticity || 0.001;
		  }
		  set friction(value) {
		    if(typeof value === 'number') {
		      this._friction = value;
		    }
		  }
		  get friction() {
		    return this._friction || 0.0085;
		  }
		}

		blob = new Blob;

		init = function() {
		  canvas = document.createElement('canvas');

		  $(".blob").prepend(canvas);

		  let resize = function() {
		    canvas.width = $(".blob").width();
		    canvas.height = $(".blob").height();
		  }
		  $(window).resize(function(){
		  	resize();
		  });
		  resize();
		  
		  let oldMousePoint = { x: 0, y: 0};
		  let hover = false;
		  let mouseMove = function(e) {
		    
		    let pos = blob.center;
		    let diff = { x: e.clientX - pos.x, y: e.clientY - pos.y };
		    let dist = Math.sqrt((diff.x * diff.x) + (diff.y * diff.y));
		    let angle = null;
		    
		    blob.mousePos = { x: pos.x - e.clientX, y: pos.y - e.clientY };
		    
		    if(dist < blob.radius && hover === false) {
		      let vector = { x: e.clientX - pos.x, y: e.clientY - pos.y };
		      angle = Math.atan2(vector.y, vector.x);
		      hover = true;
		      // blob.color = '#77FF00';
		    } else if(dist > blob.radius && hover === true){ 
		      let vector = { x: e.clientX - pos.x, y: e.clientY - pos.y };
		      angle = Math.atan2(vector.y, vector.x);
		      hover = false;
		      blob.color = null;
		    }
		    
		    if(typeof angle == 'number') {
		      
		      let nearestPoint = null;
		      let distanceFromPoint = 100;
		      
		      blob.points.forEach((point)=> {
		        if(Math.abs(angle - point.azimuth) < distanceFromPoint) {
		          // console.log(point.azimuth, angle, distanceFromPoint);
		          nearestPoint = point;
		          distanceFromPoint = Math.abs(angle - point.azimuth);
		        }
		        
		      });
		      
		      if(nearestPoint) {
		        let strength = { x: oldMousePoint.x - e.clientX, y: oldMousePoint.y - e.clientY };
		        strength = Math.sqrt((strength.x * strength.x) + (strength.y * strength.y)) * 5;
		        if(strength > 30) strength = 30;
		        nearestPoint.acceleration = strength / 70 * (hover ? -1 : 1);
		      }
		    }
		    
		    oldMousePoint.x = e.clientX;
		    oldMousePoint.y = e.clientY;
		  }
		  window.addEventListener('mousemove', mouseMove);
		  
		  blob.canvas = canvas;
		  blob.init();
		  blob.render();
		}

		init();

		////// Index half circle blob

		Math.TWO_PI = Math.PI * 2;

		const canvas2 = document.createElement('canvas');
		const context2 = canvas2.getContext('2d');

		var color;

		function loop(time) {
		    
		    loop.handle = requestAnimationFrame(loop);

		    context2.clearRect(0, 0, canvas2.width, canvas2.height);
		    
		    let angle2 = 0;
		    let count2 = 100;
		    let slice2 = Math.TWO_PI / count2;
			var radius2 = Math.min(window.innerWidth * 0.3, 250);
		    let points2 = [];
		    if($(window).width() < 991 && $(window).width() > 769) {
		    	radius2 = Math.min(window.innerWidth * 0.3, 180);
		    } else if($(window).width() < 768 ) {
		    	radius2 = Math.min(window.innerWidth * 0.3, 120);
		    } else {
		    }
		    for(let i = 0; i < count2; i++) {

		        let x = Math.cos(angle2);
		        let y = Math.sin(angle2);
		        let displacement = radius2 + (noise.simplex3(x, y, time * 0.001) * 10);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points2.push({ x, y });
		        angle2 += slice2;
		    }

		    context2.save();
		    context2.translate(canvas2.center.x, canvas2.center.y);
		    context2.beginPath();
		    context2.moveTo(points2[0].x, points2[0].y);

		    curveThrough(points2, context2);
		    var my_gradient2=context2.createLinearGradient(0,1000,1000,0);
			my_gradient2.addColorStop(0,"#3f2c75");
			my_gradient2.addColorStop(0.3,"#a20d82");
			my_gradient2.addColorStop(0.7,"#e53829");
			my_gradient2.addColorStop(1,"#c7973f");
		    context2.fillStyle = my_gradient2;
		    context2.fill();
		    context2.restore();
		}

		function resize() {
		    canvas2.width = $(".nb-info").width();
		    canvas2.height = $(".nb-info").height();
		    canvas2.center.x = $(".nb-info").width() * 0.56;
		    canvas2.center.y = $(".nb-info").height() * 0.5;
		    if($(window).width() < 991 ) {
		    	canvas2.center.x = $(".nb-info").width() * 0.7;
		    canvas2.center.y = $(".nb-info").height() * 0.25;
		    } else if($(window).width() < 900 && $(window).width() > 480) {
		    } else {
		    }
		}

		function curveThrough(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init2() {

		    canvas2.center = {};
		    noise.seed(Math.random());
		    
		    $(window).resize(function(){
			  	resize();
			  });
		    resize();
		    // color = randomColor();
			$(".nb-info").prepend(canvas2);
		    requestAnimationFrame(loop);
		}

		init2();

	});

	//////  藝 blob

	Math.TWO_PI = Math.PI * 2;

		const canvas3 = document.createElement('canvas');
		const context3 = canvas3.getContext('2d');

		var color2;
		function loop(time) {
		    
		    loop.handle = requestAnimationFrame(loop);

		    context3.clearRect(0, 0, canvas3.width, canvas3.height);
		    
		    let angle3 = 0;
		    let count3 = 100;
		    let slice3 = Math.TWO_PI / count3;
			let radius3 = Math.min(window.innerWidth * 0.3, $('.blob-menu-1').width() / 2.5);
		    let points3 = [];
		    
		    for(let i = 0; i < count3; i++) {

		        let x = Math.cos(angle3);
		        let y = Math.sin(angle3);
		        let displacement = radius3 + (noise.simplex3(x, y, time * 0.001) * 2);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points3.push({ x, y });
		        angle3 += slice3;
		    }

		    context3.save();
		    context3.translate(canvas3.center.x, canvas3.center.y);
		    context3.beginPath();
		    context3.moveTo(points3[0].x, points3[0].y);

		    curveThrough(points3, context3);
		    var my_gradient3=context3.createLinearGradient(-80,-80,40,40);
			my_gradient3.addColorStop(0,"#3f2c75");
			my_gradient3.addColorStop(0.3,"#a20d82");
			my_gradient3.addColorStop(0.7,"#e53829");
			my_gradient3.addColorStop(1,"#c7973f");
		    context3.fillStyle = my_gradient3;
		    context3.fill();
		    context3.restore();
		}

		function resize() {
		    canvas3.width = $('.blob-menu-1').width();
		    canvas3.height = $('.blob-menu-1').height();
		    canvas3.center.x = $('.blob-menu-1').width() * 0.5;
		    canvas3.center.y = $('.blob-menu-1').height() * 0.5;
		}

		function curveThrough(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init3() {

		    canvas3.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize();
			  });
		    resize();
		    // color = randomColor();
			$(".blob-menu-1 a").prepend(canvas3);
		    requestAnimationFrame(loop);
		}

		init3();

		///////  出來混 blob

		Math.TWO_PI = Math.PI * 2;

		const canvas4 = document.createElement('canvas');
		const context4 = canvas4.getContext('2d');

		function loop4(time) {
		    
		    loop4.handle = requestAnimationFrame(loop4);

		    context4.clearRect(0, 0, canvas4.width, canvas4.height);
		    
		    let angle4 = 0;
		    let count4 = 100;
		    let slice4 = Math.TWO_PI / count4;
			let radius4 = Math.min(window.innerWidth * 0.3, $('.blob-menu-2').width() / 2.5);
		    let points4 = [];
		    
		    for(let i = 0; i < count4; i++) {

		        let x = Math.cos(angle4);
		        let y = Math.sin(angle4);
		        let displacement = radius4 + (noise.simplex3(x + 1, y + 1, time * 0.001) * 2);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points4.push({ x, y });
		        angle4 += slice4;
		    }

		    context4.save();
		    context4.translate(canvas4.center.x, canvas4.center.y);
		    context4.beginPath();
		    context4.moveTo(points4[0].x, points4[0].y);

		    curveThrough4(points4, context4);
		    var my_gradient3=context4.createLinearGradient(40,40,-80,-80);
			my_gradient3.addColorStop(0,"#3f2c75");
			my_gradient3.addColorStop(0.3,"#a20d82");
			my_gradient3.addColorStop(0.7,"#e53829");
			my_gradient3.addColorStop(1,"#c7973f");
		    context4.fillStyle = my_gradient3;
		    context4.fill();
		    context4.restore();
		}

		function resize4() {
		    canvas4.width = $('.blob-menu-2').width();
		    canvas4.height = $('.blob-menu-2').height();
		    canvas4.center.x = $('.blob-menu-2').width() * 0.5;
		    canvas4.center.y = $('.blob-menu-2').height() * 0.5;
		}

		function curveThrough4(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init4() {

		    canvas4.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize4();
			  });
		    resize4();
		    // color = randomColor();
			$(".blob-menu-2 a").prepend(canvas4);
		    requestAnimationFrame(loop4);
		}

		init4();

		/////// 誰的白晝
		Math.TWO_PI = Math.PI * 2;

		const canvas5 = document.createElement('canvas');
		const context5 = canvas5.getContext('2d');

		function loop5(time) {
		    
		    loop5.handle = requestAnimationFrame(loop5);

		    context5.clearRect(0, 0, canvas5.width, canvas5.height);
		    
		    let angle5 = 0;
		    let count5 = 100;
		    let slice5 = Math.TWO_PI / count5;
			let radius5 = Math.min(window.innerWidth * 0.3, $('.blob-menu-3').width() / 2.5);
		    let points5 = [];
		    
		    for(let i = 0; i < count5; i++) {

		        let x = Math.cos(angle5);
		        let y = Math.sin(angle5);
		        let displacement = radius5 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 1.5);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points5.push({ x, y });
		        angle5 += slice5;
		    }

		    context5.save();
		    context5.translate(canvas5.center.x, canvas5.center.y);
		    context5.beginPath();
		    context5.moveTo(points5[0].x, points5[0].y);

		    curveThrough5(points5, context5);
		    var my_gradient5=context5.createLinearGradient(0,-80,40,40);
			my_gradient5.addColorStop(0,"#3f2c75");
			my_gradient5.addColorStop(0.4,"#a20d82");
			my_gradient5.addColorStop(1,"#e53829");
		    context5.fillStyle = my_gradient5;
		    context5.fill();
		    context5.restore();
		}

		function resize5() {
		    canvas5.width = $(".blob-menu-3").width();
		    canvas5.height = $(".blob-menu-3").height();
		    canvas5.center.x = $(".blob-menu-3").width() * 0.5;
		    canvas5.center.y = $(".blob-menu-3").height() * 0.5;
		}

		function curveThrough5(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init5() {

		    canvas5.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize5();
			  });
		    resize5();
		    // color = randomColor();
			$(".blob-menu-3 a").prepend(canvas5);
		    requestAnimationFrame(loop5);
		}

		init5();

		/////// Dresscode blob
		Math.TWO_PI = Math.PI * 2;

		const canvas6 = document.createElement('canvas');
		const context6 = canvas6.getContext('2d');

		function loop6(time) {
		    
		    loop6.handle = requestAnimationFrame(loop6);

		    context6.clearRect(0, 0, canvas6.width, canvas6.height);
		    
		    let angle6 = 0;
		    let count6 = 100;
		    let slice6 = Math.TWO_PI / count6;
			let radius6 = Math.min(window.innerWidth * 0.3, $('.blob-menu-4').width() / 2.5);
		    let points6 = [];
		    
		    for(let i = 0; i < count6; i++) {

		        let x = Math.cos(angle6);
		        let y = Math.sin(angle6);
		        let displacement = radius6 + (noise.simplex3(x + 4, y + 2, time * 0.001) * 2.3);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points6.push({ x, y });
		        angle6 += slice6;
		    }

		    context6.save();
		    context6.translate(canvas6.center.x, canvas6.center.y);
		    context6.beginPath();
		    context6.moveTo(points6[0].x, points6[0].y);

		    curveThrough5(points6, context6);
		    var my_gradient6=context6.createLinearGradient(0,-80,0,40);
			my_gradient6.addColorStop(0,"#3f2c75");
			my_gradient6.addColorStop(0.3,"#a20d82");
			my_gradient6.addColorStop(0.7,"#e53829");
			my_gradient6.addColorStop(1,"#c7973f");
		    context6.fillStyle = my_gradient6;
		    context6.fill();
		    context6.restore();
		}

		function resize6() {
		    canvas6.width = $(".blob-menu-4").width();
		    canvas6.height = $(".blob-menu-4").height();
		    canvas6.center.x = $(".blob-menu-4").width() * 0.5;
		    canvas6.center.y = $(".blob-menu-4").height() * 0.5;
		}

		function curveThrough5(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init6() {

		    canvas6.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize6();
			  });
		    resize6();
		    // color = randomColor();
			$(".blob-menu-4 a").prepend(canvas6);
		    requestAnimationFrame(loop6);
		}

		init6();

		////// 在地場域 blob
		Math.TWO_PI = Math.PI * 2;

		const canvas7 = document.createElement('canvas');
		const context7 = canvas7.getContext('2d');

		function loop7(time) {
		    
		    loop7.handle = requestAnimationFrame(loop7);

		    context7.clearRect(0, 0, canvas7.width, canvas7.height);
		    
		    let angle7 = 0;
		    let count7 = 100;
		    let slice7 = Math.TWO_PI / count7;
			let radius7 = Math.min(window.innerWidth * 0.3, $('.blob-menu-5').width() / 2.7);
		    let points7 = [];
		    
		    for(let i = 0; i < count7; i++) {

		        let x = Math.cos(angle7);
		        let y = Math.sin(angle7);
		        let displacement = radius7 + (noise.simplex3(x, y, time * 0.0003) * 6);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points7.push({ x, y });
		        angle7 += slice7;
		    }

		    context7.save();
		    context7.translate(canvas7.center.x, canvas7.center.y);
		    context7.beginPath();
		    context7.moveTo(points7[0].x, points7[0].y);

		    curveThrough7(points7, context7);
		    var my_gradient7=context7.createLinearGradient(0,160,200,50);
			my_gradient7.addColorStop(0,"#3f2c75");
			my_gradient7.addColorStop(0.3,"#a20d82");
			my_gradient7.addColorStop(1,"#e53829");
			// my_gradient7.addColorStop(1,"#c7973f");
		    context7.fillStyle = my_gradient7;
		    context7.fill();
		    context7.restore();
		}

		function resize7() {
		    canvas7.width = $(".blob-menu-5").width();
		    canvas7.height = $(".blob-menu-5").height();
		    canvas7.center.x = $(".blob-menu-5").width() * 0.5;
		    canvas7.center.y = $(".blob-menu-5").height() * 0.5;
		}

		function curveThrough7(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init7() {

		    canvas7.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize7();
			  });
		    resize7();
		    // color = randomColor();
			$(".blob-menu-5 a").prepend(canvas7);
		    requestAnimationFrame(loop7);
		}

		init7();


		////// 企 blob
		Math.TWO_PI = Math.PI * 2;

		const canvas8 = document.createElement('canvas');
		const context8 = canvas8.getContext('2d');

		function loop8(time) {
		    
		    loop8.handle = requestAnimationFrame(loop8);

		    context8.clearRect(0, 0, canvas8.width, canvas8.height);
		    
		    let angle8 = 0;
		    let count8 = 100;
		    let slice8 = Math.TWO_PI / count8;
			let radius8 = Math.min(window.innerWidth * 0.3, $('.blob-menu-6').width() / 2.5);
		    let points8 = [];
		    
		    for(let i = 0; i < count8; i++) {

		        let x = Math.cos(angle8);
		        let y = Math.sin(angle8);
		        let displacement = radius8 + (noise.simplex3(x + 5, y + 5, time * 0.001) * 2);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points8.push({ x, y });
		        angle8 += slice8;
		    }

		    context8.save();
		    context8.translate(canvas8.center.x, canvas8.center.y);
		    context8.beginPath();
		    context8.moveTo(points8[0].x, points8[0].y);

		    curveThrough8(points8, context8);
		    var my_gradient8=context8.createLinearGradient(40,80,-40,-80);
			my_gradient8.addColorStop(0,"#3f2c75");
			my_gradient8.addColorStop(0.3,"#a20d82");
			my_gradient8.addColorStop(0.7,"#e53829");
			my_gradient8.addColorStop(1,"#c7973f");
		    context8.fillStyle = my_gradient8;
		    context8.fill();
		    context8.restore();
		}

		function resize8() {
		    canvas8.width = $(".blob-menu-6").width();
		    canvas8.height = $(".blob-menu-6").height();
		    canvas8.center.x = $(".blob-menu-6").width() * 0.5;
		    canvas8.center.y = $(".blob-menu-6").height() * 0.5;
		}

		function curveThrough8(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init8() {

		    canvas8.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize8();
			  });
		    resize8();
		    // color = randomColor();
			$(".blob-menu-6 a").prepend(canvas8);
		    requestAnimationFrame(loop8);
		}

		init8();

		/////// 民眾參與 blob
		Math.TWO_PI = Math.PI * 2;

		const canvas9 = document.createElement('canvas');
		const context9 = canvas9.getContext('2d');

		function loop9(time) {
		    
		    loop9.handle = requestAnimationFrame(loop9);

		    context9.clearRect(0, 0, canvas9.width, canvas9.height);
		    
		    let angle9 = 0;
		    let count9 = 100;
		    let slice9 = Math.TWO_PI / count9;
			let radius9 = Math.min(window.innerWidth * 0.3, $('.blob-menu-7').width() / 2.5);
		    let points9 = [];
		    
		    for(let i = 0; i < count9; i++) {

		        let x = Math.cos(angle9);
		        let y = Math.sin(angle9);
		        let displacement = radius9 + (noise.simplex3(x + 7, y + 3, time * 0.0003) * 8);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points9.push({ x, y });
		        angle9 += slice9;
		    }

		    context9.save();
		    context9.translate(canvas9.center.x, canvas9.center.y);
		    context9.beginPath();
		    context9.moveTo(points9[0].x, points9[0].y);

		    curveThrough9(points9, context9);
		    var my_gradient9=context9.createLinearGradient(-120,60,120,0);
			my_gradient9.addColorStop(0,"#c7973f");
			my_gradient9.addColorStop(0.3,"#a20d82");
			my_gradient9.addColorStop(0.7,"#3f2c75");
			my_gradient9.addColorStop(1,"#e53829");
		    context9.fillStyle = my_gradient9;
		    context9.fill();
		    context9.restore();
		}

		function resize9() {
		    canvas9.width = $(".blob-menu-7").width();
		    canvas9.height = $(".blob-menu-7").height();
		    canvas9.center.x = $(".blob-menu-7").width() * 0.5;
		    canvas9.center.y = $(".blob-menu-7").height() * 0.5;
		}

		function curveThrough9(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init9() {

		    canvas9.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize9();
			  });
		    resize9();
		    // color = randomColor();
			$(".blob-menu-7 a").prepend(canvas9);
		    requestAnimationFrame(loop9);
		}

		init9();

		/////// 全球性 blob
		Math.TWO_PI = Math.PI * 2;

		const canvas10 = document.createElement('canvas');
		const context10 = canvas10.getContext('2d');

		function loop10(time) {
		    
		    loop10.handle = requestAnimationFrame(loop10);

		    context10.clearRect(0, 0, canvas10.width, canvas10.height);
		    
		    let angle10 = 0;
		    let count10 = 100;
		    let slice10 = Math.TWO_PI / count10;
			let radius10 = Math.min(window.innerWidth * 0.3, $('.blob-menu-8').width() / 2.5);
		    let points10 = [];
		    
		    for(let i = 0; i < count10; i++) {

		        let x = Math.cos(angle10);
		        let y = Math.sin(angle10);
		        let displacement = radius10 + (noise.simplex3(x, y, time * 0.001) * 2);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points10.push({ x, y });
		        angle10 += slice10;
		    }

		    context10.save();
		    context10.translate(canvas10.center.x, canvas10.center.y);
		    context10.beginPath();
		    context10.moveTo(points10[0].x, points10[0].y);

		    curveThrough10(points10, context10);
		    var my_gradient10=context10.createLinearGradient(80,-80,-40,40);
			my_gradient10.addColorStop(0,"#3f2c75");
			my_gradient10.addColorStop(0.3,"#a20d82");
			my_gradient10.addColorStop(0.7,"#e53829");
			my_gradient10.addColorStop(1,"#c7973f");
		    context10.fillStyle = my_gradient10;
		    context10.fill();
		    context10.restore();
		}

		function resize10() {
		    canvas10.width = $(".blob-menu-8").width();
		    canvas10.height = $(".blob-menu-8").height();
		    canvas10.center.x = $(".blob-menu-8").width() * 0.5;
		    canvas10.center.y = $(".blob-menu-8").height() * 0.5;
		}

		function curveThrough10(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init10() {

		    canvas10.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize10();
			  });
		    resize10();
		    // color = randomColor();
			$(".blob-menu-8 a").prepend(canvas10);
		    requestAnimationFrame(loop10);
		}

		init10();

		/////// 中山北裡 blob
		Math.TWO_PI = Math.PI * 2;

		const canvas11 = document.createElement('canvas');
		const context11 = canvas11.getContext('2d');

		function loop11(time) {
		    
		    loop11.handle = requestAnimationFrame(loop11);

		    context11.clearRect(0, 0, canvas11.width, canvas11.height);
		    
		    let angle11 = 0;
		    let count11 = 100;
		    let slice11 = Math.TWO_PI / count11;
			let radius11 = Math.min(window.innerWidth * 0.3, $('.blob-menu-9').width() / 2.5);
		    let points11 = [];
		    
		    for(let i = 0; i < count11; i++) {

		        let x = Math.cos(angle11);
		        let y = Math.sin(angle11);
		        let displacement = radius11 + (noise.simplex3(x, y, time * 0.001) * 2);
		        
		        x = x * displacement;
		        y = y * displacement;
		        
		        points11.push({ x, y });
		        angle11 += slice11;
		    }

		    context11.save();
		    context11.translate(canvas11.center.x, canvas11.center.y);
		    context11.beginPath();
		    context11.moveTo(points11[0].x, points11[0].y);

		    curveThrough11(points11, context11);
		    var my_gradient11=context11.createLinearGradient(-80,-80,150,0);
			my_gradient11.addColorStop(0,"#c7973f");
			my_gradient11.addColorStop(0.3,"#e53829");
			my_gradient11.addColorStop(0.7,"#a20d82");
			my_gradient11.addColorStop(1,"#3f2c75");
		    context11.fillStyle = my_gradient11;
		    context11.fill();
		    context11.restore();
		}

		function resize11() {
		    canvas11.width = $(".blob-menu-9").width();
		    canvas11.height = $(".blob-menu-9").height();
		    canvas11.center.x = $(".blob-menu-9").width() * 0.5;
		    canvas11.center.y = $(".blob-menu-9").height() * 0.5;
		}

		function curveThrough11(points, context) {

		    var i, n, a, b, x, y;
			
		    for (i = 1, n = points.length - 2; i < n; i++) {

		        a = points[i];
		        b = points[i + 1];
		        x = (a.x + b.x) * 0.5;
		        y = (a.y + b.y) * 0.5;

		        context.quadraticCurveTo(a.x, a.y, x, y);
		    }

		    a = points[i];
		    b = points[i + 1];

		    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
		}

		function init11() {

		    canvas11.center = {};
		    noise.seed(Math.random());
		    $(window).resize(function(){
			  	resize11();
			  });
		    resize11();
		    // color = randomColor();
			$(".blob-menu-9 a").prepend(canvas11);
		    requestAnimationFrame(loop11);
		}

		init11();
		
}

function initBlobMenu() {
	initGlobal();
}
function initTimeline() {
	initGlobal();
	var $el, leftPos, newWidth,
        $mainNav = $(".timeline-group");
    $mainNav.append("<div id='pointer'></div>");
    var $pointer = $("#pointer");
    var leftOffset = $(".current-timeline").width() / 2 - 4;
    $pointer
        .css("left", $(".current-timeline").position().left + leftOffset)
        .data("origLeft", $pointer.position().left);
    $(".timelines").hover(function() {
    	var leftOffset = $(this).width() / 2 - 4;
        $el = $(this);
        leftPos = $el.position().left + leftOffset;
        $pointer.stop().animate({
            left: leftPos
        });
    }, function() {
        $pointer.stop().animate({
            left: $pointer.data("origLeft")
        });
    });
    $(".timelines").each(function() {
    	$(this).click(function(){
    		$(".timelines").removeClass('current-timeline');
    		$(this).addClass('current-timeline');
    		var leftOffset = $(".current-timeline").width() / 2 - 4;
    		$pointer
				.css("left", $(".current-timeline").position().left + leftOffset)
				.data("origLeft", $pointer.position().left);
			var timeDot = $(this).attr('time-tag');
			$(".timeline-box").fadeOut().removeClass('current-timeline-box');
			$(".timeline-box[time-box=" + timeDot + "]").addClass('current-timeline-box').fadeIn();
			$(window).scrollTop(0);
    	});
    });
    $(window).resize(function() {
    	var leftOffset = $(".current-timeline").width() / 2;
		 $pointer
	        .css("left", $(".current-timeline").position().left + leftOffset)
	        .data("origLeft", $pointer.position().left);
    });
}

function initRoute() {
	initGlobal();
	///// Route Blob 1
	Math.TWO_PI = Math.PI * 2;

	const canvasRoute1 = document.createElement('canvas');
	const contextRoute1 = canvasRoute1.getContext('2d');

	function loopRoute1(time) {
	    
	    loopRoute1.handle = requestAnimationFrame(loopRoute1);

	    contextRoute1.clearRect(0, 0, canvasRoute1.width, canvasRoute1.height);
	    
	    let angleRoute1 = 0;
	    let countRoute1 = 100;
	    let sliceRoute1 = Math.TWO_PI / countRoute1;
		let radiusRoute1 = Math.min(window.innerWidth * 0.3, $('.route-blob-1').width() / 2.5);
	    let pointsRoute1 = [];
	    
	    for(let i = 0; i < countRoute1; i++) {

	        let x = Math.cos(angleRoute1);
	        let y = Math.sin(angleRoute1);
	        let displacement = radiusRoute1 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 3);
	        
	        x = x * displacement;
	        y = y * displacement;
	        
	        pointsRoute1.push({ x, y });
	        angleRoute1 += sliceRoute1;
	    }

	    contextRoute1.save();
	    contextRoute1.translate(canvasRoute1.center.x, canvasRoute1.center.y);
	    contextRoute1.beginPath();
	    contextRoute1.moveTo(pointsRoute1[0].x, pointsRoute1[0].y);

	    curveThroughRoute1(pointsRoute1, contextRoute1);
	    var my_gradientRoute1=contextRoute1.createLinearGradient(0,-80,40,40);
		my_gradientRoute1.addColorStop(0,"#3f2c75");
		my_gradientRoute1.addColorStop(0.4,"#a20d82");
		my_gradientRoute1.addColorStop(1,"#e53829");
	    contextRoute1.fillStyle = my_gradientRoute1;
	    contextRoute1.fill();
	    contextRoute1.restore();
	}

	function resizeRoute1() {
	    canvasRoute1.width = $(".route-blob-1").width();
	    canvasRoute1.height = $(".route-blob-1").height();
	    canvasRoute1.center.x = $(".route-blob-1").width() * 0.5;
	    canvasRoute1.center.y = $(".route-blob-1").height() * 0.5;
	}

	function curveThroughRoute1(points, context) {

	    var i, n, a, b, x, y;
		
	    for (i = 1, n = points.length - 2; i < n; i++) {

	        a = points[i];
	        b = points[i + 1];
	        x = (a.x + b.x) * 0.5;
	        y = (a.y + b.y) * 0.5;

	        context.quadraticCurveTo(a.x, a.y, x, y);
	    }

	    a = points[i];
	    b = points[i + 1];

	    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
	}

	function initRoute1() {

	    canvasRoute1.center = {};
	    noise.seed(Math.random());
	    $(window).resize(function(){
		  	resizeRoute1();
		  });
	    resizeRoute1();
	    // color = randomColor();
		$(".route-blob-1").prepend(canvasRoute1);
	    requestAnimationFrame(loopRoute1);
	}

	initRoute1();

	//// Route Blob 2

	Math.TWO_PI = Math.PI * 2;

	const canvasRoute2 = document.createElement('canvas');
	const contextRoute2 = canvasRoute2.getContext('2d');

	function loopRoute2(time) {
	    
	    loopRoute2.handle = requestAnimationFrame(loopRoute2);

	    contextRoute2.clearRect(0, 0, canvasRoute2.width, canvasRoute2.height);
	    
	    let angleRoute2 = 0;
	    let countRoute2 = 100;
	    let sliceRoute2 = Math.TWO_PI / countRoute2;
		let radiusRoute2 = Math.min(window.innerWidth * 0.3, $('.route-blob-2').width() / 2.5);
	    let pointsRoute2 = [];
	    
	    for(let i = 0; i < countRoute2; i++) {

	        let x = Math.cos(angleRoute2);
	        let y = Math.sin(angleRoute2);
	        let displacement = radiusRoute2 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 4);
	        
	        x = x * displacement;
	        y = y * displacement;
	        
	        pointsRoute2.push({ x, y });
	        angleRoute2 += sliceRoute2;
	    }

	    contextRoute2.save();
	    contextRoute2.translate(canvasRoute2.center.x, canvasRoute2.center.y);
	    contextRoute2.beginPath();
	    contextRoute2.moveTo(pointsRoute2[0].x, pointsRoute2[0].y);

	    curveThroughRoute2(pointsRoute2, contextRoute2);
	    var my_gradientRoute2=contextRoute2.createLinearGradient(0,-80,40,40);
		my_gradientRoute2.addColorStop(0,"#3f2c75");
		my_gradientRoute2.addColorStop(0.4,"#a20d82");
		my_gradientRoute2.addColorStop(1,"#e53829");
	    contextRoute2.fillStyle = my_gradientRoute2;
	    contextRoute2.fill();
	    contextRoute2.restore();
	}

	function resizeRoute2() {
	    canvasRoute2.width = $(".route-blob-2").width();
	    canvasRoute2.height = $(".route-blob-2").height();
	    canvasRoute2.center.x = $(".route-blob-2").width() * 0.5;
	    canvasRoute2.center.y = $(".route-blob-2").height() * 0.5;
	}

	function curveThroughRoute2(points, context) {

	    var i, n, a, b, x, y;
		
	    for (i = 1, n = points.length - 2; i < n; i++) {

	        a = points[i];
	        b = points[i + 1];
	        x = (a.x + b.x) * 0.5;
	        y = (a.y + b.y) * 0.5;

	        context.quadraticCurveTo(a.x, a.y, x, y);
	    }

	    a = points[i];
	    b = points[i + 1];

	    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
	}

	function initRoute2() {

	    canvasRoute2.center = {};
	    noise.seed(Math.random());
	    $(window).resize(function(){
		  	resizeRoute2();
		  });
	    resizeRoute2();
	    // color = randomColor();
		$(".route-blob-2").prepend(canvasRoute2);
	    requestAnimationFrame(loopRoute2);
	}

	initRoute2();
}
function initProgram() {
	initGlobal();
	$(".program-filter").each(function(){
		$(this).click(function(){
			$(".program-filter").removeClass('program-filter-active');
			$(this).addClass('program-filter-active');
		});
	});
	$(".pages").each(function() {
		$(this).click(function(){
			$(".pages").removeClass('active-page');
			$(this).addClass('active-page');
		});
	});
	///// Route Blob 1
	Math.TWO_PI = Math.PI * 2;

	const canvasRoute1 = document.createElement('canvas');
	const contextRoute1 = canvasRoute1.getContext('2d');

	function loopRoute1(time) {
	    
	    loopRoute1.handle = requestAnimationFrame(loopRoute1);

	    contextRoute1.clearRect(0, 0, canvasRoute1.width, canvasRoute1.height);
	    
	    let angleRoute1 = 0;
	    let countRoute1 = 100;
	    let sliceRoute1 = Math.TWO_PI / countRoute1;
		let radiusRoute1 = Math.min(window.innerWidth * 0.3, $('.route-blob-1').width() / 2.5);
	    let pointsRoute1 = [];
	    
	    for(let i = 0; i < countRoute1; i++) {

	        let x = Math.cos(angleRoute1);
	        let y = Math.sin(angleRoute1);
	        let displacement = radiusRoute1 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 3);
	        
	        x = x * displacement;
	        y = y * displacement;
	        
	        pointsRoute1.push({ x, y });
	        angleRoute1 += sliceRoute1;
	    }

	    contextRoute1.save();
	    contextRoute1.translate(canvasRoute1.center.x, canvasRoute1.center.y);
	    contextRoute1.beginPath();
	    contextRoute1.moveTo(pointsRoute1[0].x, pointsRoute1[0].y);

	    curveThroughRoute1(pointsRoute1, contextRoute1);
	    var my_gradientRoute1=contextRoute1.createLinearGradient(0,-80,40,40);
		my_gradientRoute1.addColorStop(0,"#3f2c75");
		my_gradientRoute1.addColorStop(0.4,"#a20d82");
		my_gradientRoute1.addColorStop(1,"#e53829");
	    contextRoute1.fillStyle = my_gradientRoute1;
	    contextRoute1.fill();
	    contextRoute1.restore();
	}

	function resizeRoute1() {
	    canvasRoute1.width = $(".route-blob-1").width();
	    canvasRoute1.height = $(".route-blob-1").height();
	    canvasRoute1.center.x = $(".route-blob-1").width() * 0.5;
	    canvasRoute1.center.y = $(".route-blob-1").height() * 0.5;
	}

	function curveThroughRoute1(points, context) {

	    var i, n, a, b, x, y;
		
	    for (i = 1, n = points.length - 2; i < n; i++) {

	        a = points[i];
	        b = points[i + 1];
	        x = (a.x + b.x) * 0.5;
	        y = (a.y + b.y) * 0.5;

	        context.quadraticCurveTo(a.x, a.y, x, y);
	    }

	    a = points[i];
	    b = points[i + 1];

	    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
	}

	function initRoute1() {

	    canvasRoute1.center = {};
	    noise.seed(Math.random());
	    $(window).resize(function(){
		  	resizeRoute1();
		  });
	    resizeRoute1();
	    // color = randomColor();
		$(".route-blob-1").prepend(canvasRoute1);
	    requestAnimationFrame(loopRoute1);
	}

	initRoute1();

	//// Route Blob 2

	Math.TWO_PI = Math.PI * 2;

	const canvasRoute2 = document.createElement('canvas');
	const contextRoute2 = canvasRoute2.getContext('2d');

	function loopRoute2(time) {
	    
	    loopRoute2.handle = requestAnimationFrame(loopRoute2);

	    contextRoute2.clearRect(0, 0, canvasRoute2.width, canvasRoute2.height);
	    
	    let angleRoute2 = 0;
	    let countRoute2 = 100;
	    let sliceRoute2 = Math.TWO_PI / countRoute2;
		let radiusRoute2 = Math.min(window.innerWidth * 0.3, $('.route-blob-2').width() / 2.5);
	    let pointsRoute2 = [];
	    
	    for(let i = 0; i < countRoute2; i++) {

	        let x = Math.cos(angleRoute2);
	        let y = Math.sin(angleRoute2);
	        let displacement = radiusRoute2 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 4);
	        
	        x = x * displacement;
	        y = y * displacement;
	        
	        pointsRoute2.push({ x, y });
	        angleRoute2 += sliceRoute2;
	    }

	    contextRoute2.save();
	    contextRoute2.translate(canvasRoute2.center.x, canvasRoute2.center.y);
	    contextRoute2.beginPath();
	    contextRoute2.moveTo(pointsRoute2[0].x, pointsRoute2[0].y);

	    curveThroughRoute2(pointsRoute2, contextRoute2);
	    var my_gradientRoute2=contextRoute2.createLinearGradient(0,-80,40,40);
		my_gradientRoute2.addColorStop(0,"#3f2c75");
		my_gradientRoute2.addColorStop(0.4,"#a20d82");
		my_gradientRoute2.addColorStop(1,"#e53829");
	    contextRoute2.fillStyle = my_gradientRoute2;
	    contextRoute2.fill();
	    contextRoute2.restore();
	}

	function resizeRoute2() {
	    canvasRoute2.width = $(".route-blob-2").width();
	    canvasRoute2.height = $(".route-blob-2").height();
	    canvasRoute2.center.x = $(".route-blob-2").width() * 0.5;
	    canvasRoute2.center.y = $(".route-blob-2").height() * 0.5;
	}

	function curveThroughRoute2(points, context) {

	    var i, n, a, b, x, y;
		
	    for (i = 1, n = points.length - 2; i < n; i++) {

	        a = points[i];
	        b = points[i + 1];
	        x = (a.x + b.x) * 0.5;
	        y = (a.y + b.y) * 0.5;

	        context.quadraticCurveTo(a.x, a.y, x, y);
	    }

	    a = points[i];
	    b = points[i + 1];

	    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
	}

	function initRoute2() {

	    canvasRoute2.center = {};
	    noise.seed(Math.random());
	    $(window).resize(function(){
		  	resizeRoute2();
		  });
	    resizeRoute2();
	    // color = randomColor();
		$(".route-blob-2").prepend(canvasRoute2);
	    requestAnimationFrame(loopRoute2);
	}

	initRoute2();
}
function initCategory() {
	initGlobal();
	function scheduleShow() {
		var scheduleTime = $(".time-schedule-date .active-schedule").attr("schedule-tag");
		$(".time-schedule-content-wrap .row").each(function(){
			if($(this).attr("schedule-time") == scheduleTime) {
				$(".time-schedule-content-wrap .row").removeClass('time-schedule-content-active');
				$(this).addClass('time-schedule-content-active');
			}
		});
	}
	scheduleShow();
	$(".time-schedule-date p").each(function(){
		$(this).click(function(){
			$(".time-schedule-date p").removeClass('active-schedule');
			$(this).addClass('active-schedule');
			scheduleShow();
		});
	});
	var len = 80;
	$('.time-schedule-content-description-p').each(function() {
		if($(this).text().length>len){
            $(this).attr("title",$(this).text());
            var text=$(this).text().substring(0,len-1)+"...";
            $(this).text(text);
        }
	});
	Math.TWO_PI = Math.PI * 2;

	const canvasRoute1 = document.createElement('canvas');
	const contextRoute1 = canvasRoute1.getContext('2d');

	function loopRoute1(time) {
	    
	    loopRoute1.handle = requestAnimationFrame(loopRoute1);

	    contextRoute1.clearRect(0, 0, canvasRoute1.width, canvasRoute1.height);
	    
	    let angleRoute1 = 0;
	    let countRoute1 = 100;
	    let sliceRoute1 = Math.TWO_PI / countRoute1;
		let radiusRoute1 = Math.min(window.innerWidth * 0.3, $('.route-blob-1').width() / 2.5);
	    let pointsRoute1 = [];
	    
	    for(let i = 0; i < countRoute1; i++) {

	        let x = Math.cos(angleRoute1);
	        let y = Math.sin(angleRoute1);
	        let displacement = radiusRoute1 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 1.5);
	        
	        x = x * displacement;
	        y = y * displacement;
	        
	        pointsRoute1.push({ x, y });
	        angleRoute1 += sliceRoute1;
	    }

	    contextRoute1.save();
	    contextRoute1.translate(canvasRoute1.center.x, canvasRoute1.center.y);
	    contextRoute1.beginPath();
	    contextRoute1.moveTo(pointsRoute1[0].x, pointsRoute1[0].y);

	    curveThroughRoute1(pointsRoute1, contextRoute1);
	    var my_gradientRoute1=contextRoute1.createLinearGradient(0,-80,40,40);
		my_gradientRoute1.addColorStop(0,"#3f2c75");
		my_gradientRoute1.addColorStop(0.4,"#a20d82");
		my_gradientRoute1.addColorStop(1,"#e53829");
	    contextRoute1.fillStyle = my_gradientRoute1;
	    contextRoute1.fill();
	    contextRoute1.restore();
	}

	function resizeRoute1() {
	    canvasRoute1.width = $(".route-blob-1").width();
	    canvasRoute1.height = $(".route-blob-1").height();
	    canvasRoute1.center.x = $(".route-blob-1").width() * 0.5;
	    canvasRoute1.center.y = $(".route-blob-1").height() * 0.5;
	}

	function curveThroughRoute1(points, context) {

	    var i, n, a, b, x, y;
		
	    for (i = 1, n = points.length - 2; i < n; i++) {

	        a = points[i];
	        b = points[i + 1];
	        x = (a.x + b.x) * 0.5;
	        y = (a.y + b.y) * 0.5;

	        context.quadraticCurveTo(a.x, a.y, x, y);
	    }

	    a = points[i];
	    b = points[i + 1];

	    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
	}

	function initRoute1() {

	    canvasRoute1.center = {};
	    noise.seed(Math.random());
	    $(window).resize(function(){
		  	resizeRoute1();
		  });
	    resizeRoute1();
	    // color = randomColor();
		$(".route-blob-1").prepend(canvasRoute1);
	    requestAnimationFrame(loopRoute1);
	}

	initRoute1();

	//// Route Blob 2

	Math.TWO_PI = Math.PI * 2;

	const canvasRoute2 = document.createElement('canvas');
	const contextRoute2 = canvasRoute2.getContext('2d');

	function loopRoute2(time) {
	    
	    loopRoute2.handle = requestAnimationFrame(loopRoute2);

	    contextRoute2.clearRect(0, 0, canvasRoute2.width, canvasRoute2.height);
	    
	    let angleRoute2 = 0;
	    let countRoute2 = 100;
	    let sliceRoute2 = Math.TWO_PI / countRoute2;
		let radiusRoute2 = Math.min(window.innerWidth * 0.3, $('.route-blob-2').width() / 2.5);
	    let pointsRoute2 = [];
	    
	    for(let i = 0; i < countRoute2; i++) {

	        let x = Math.cos(angleRoute2);
	        let y = Math.sin(angleRoute2);
	        let displacement = radiusRoute2 + (noise.simplex3(x + 1.3, y + 1.3, time * 0.001) * 1.5);
	        
	        x = x * displacement;
	        y = y * displacement;
	        
	        pointsRoute2.push({ x, y });
	        angleRoute2 += sliceRoute2;
	    }

	    contextRoute2.save();
	    contextRoute2.translate(canvasRoute2.center.x, canvasRoute2.center.y);
	    contextRoute2.beginPath();
	    contextRoute2.moveTo(pointsRoute2[0].x, pointsRoute2[0].y);

	    curveThroughRoute2(pointsRoute2, contextRoute2);
	    var my_gradientRoute2=contextRoute2.createLinearGradient(0,-80,40,40);
		my_gradientRoute2.addColorStop(0,"#3f2c75");
		my_gradientRoute2.addColorStop(0.4,"#a20d82");
		my_gradientRoute2.addColorStop(1,"#e53829");
	    contextRoute2.fillStyle = my_gradientRoute2;
	    contextRoute2.fill();
	    contextRoute2.restore();
	}

	function resizeRoute2() {
	    canvasRoute2.width = $(".route-blob-2").width();
	    canvasRoute2.height = $(".route-blob-2").height();
	    canvasRoute2.center.x = $(".route-blob-2").width() * 0.5;
	    canvasRoute2.center.y = $(".route-blob-2").height() * 0.5;
	}

	function curveThroughRoute2(points, context) {

	    var i, n, a, b, x, y;
		
	    for (i = 1, n = points.length - 2; i < n; i++) {

	        a = points[i];
	        b = points[i + 1];
	        x = (a.x + b.x) * 0.5;
	        y = (a.y + b.y) * 0.5;

	        context.quadraticCurveTo(a.x, a.y, x, y);
	    }

	    a = points[i];
	    b = points[i + 1];

	    context.quadraticCurveTo(a.x, a.y, b.x, b.y);
	}

	function initRoute2() {

	    canvasRoute2.center = {};
	    noise.seed(Math.random());
	    $(window).resize(function(){
		  	resizeRoute2();
		  });
	    resizeRoute2();
	    // color = randomColor();
		$(".route-blob-2").prepend(canvasRoute2);
	    requestAnimationFrame(loopRoute2);
	}

	initRoute2();
}
function initWork() {
	initGlobal();
	$(".thumbnail-img").each(function(){
		$(this).click(function(){
			var control = $(this).attr("control-tag");
			$(".carousel-item").removeClass('active');
			$(".carousel-item:eq("+control+")").addClass('active');
			$(".thumbnail-img").removeClass('thumbnail-active');
			$(this).addClass('thumbnail-active');
		});
	});
	setInterval(function(){
		var slideNo = $(".carousel-item.active .d-block").attr("alt");
		$(".thumbnail-img").each(function(){
			if($(this).attr("control-slide") == slideNo) {
				$(".thumbnail-img").removeClass('thumbnail-active');
				$(this).addClass('thumbnail-active');
			}
		});
	}, 500);
}
function initAbout() {
	initGlobal();
	var emptyCells, i;
	$('.partner-align').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.partner-block').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'partner-block is-empty'
			}));
		}
		$(this).append(emptyCells);
	});
	$('.world-block-wrap').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.world-block').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'world-block is-empty'
			}));
		}
		$(this).append(emptyCells);
	});
}
function initAccess() {
	initGlobal();
	initialize2();
	var emptyCells, i;
	$('.mrt-station-block').each(function() {
		emptyCells = [];
		for (i = 0; i < $(this).find('.mrt-station').length; i++) {
			emptyCells.push($('<div>', {
			  class: 'mrt-station is-empty'
			}));
		}
		$(this).append(emptyCells);
	});
}